/**
 * The updateContacts function uses the list of contacts objects
 * to create the table that is displayed on the left side of the
 * web page
 */
function updateContacts(){
    //use the sortByLast helper function to sort the contacts by their last name
    contacts.sort(sortByLast);

    contactHTML = "<table style='width:60%;float:left'>";

    lastInitial = "";
    previousLast = "";

    for(var i = 0; i < contacts.length; i++){
        lastInitial = contacts[i].last.charAt(0);

        //check to see if the next contact's first initial matches the previous contact
        //if the initials are different, we need to make a header for the new initial
        if(lastInitial !== previousLast){
            contactHTML += "<tr class='contact-header' id=" + lastInitial + "><td>" + lastInitial + "</td></tr>";
        }

        //every other row will be shaded gray
        if(i % 2 === 0) {
            contactHTML += "<tr><td class='contact-row'><a onclick=showContact(\'" + JSON.stringify(contacts[i]) + "\')>" + contacts[i].last + ", " + contacts[i].first + "</a></td></tr>";
        } else {
            contactHTML += "<tr class='odd-row'><td class='contact-row'><a onclick=showContact(\'" + JSON.stringify(contacts[i]) + "\')>" + contacts[i].last + ", " + contacts[i].first + "</a></td></tr>";
        }

        previousLast = lastInitial;
    }

    contactHTML += "</table>";

    //update the contents of the "contact_list" div element with this table html
    contactDiv = document.getElementById("contact_list");
    contactDiv.innerHTML = contactHTML;
}

/*
 *sortByLast is a helper function that I am using to sort the contacts array by last name
 */
function sortByLast(con1, con2){
    if(con1.last < con2.last) {
        return -1;
    }
    else if(con1.last > con2.last){
        return 1;
    }
    else{
        return 0;
    }

}

/*
 * The showContact function is used when the user clicks on a contact name.
 * The function displays a small table on the right side of the screen
 */
function showContact(contactString){
    contactJson = JSON.parse(contactString);
    detailsDiv = document.getElementById("contact_details");
    //detailsDiv.innerHTML = "this is a test1";
    // document.write("test 2");
    detailsDiv.innerHTML = "<table class='single-contact'><tr><td class='contact-details'>Name: " + contactJson.first + " " + contactJson.last + "</td></tr><tr><td class='contact-details'>Email: " + contactJson.email + "</td></tr><tr><td class='contact-details'>Phone: " + contactJson.phone + "</td></tr>";
}

/*
 * The addNewContact function runs when the user clicks the "Add New Contact" button
 * at the top of tha page.  We'll grab the new user's info through prompts, make sure the user
 * is not already in the contact list, and then push into the contacts array
 */
function addNewContact()
{
    var firstName = prompt("Enter your first name:");
    var lastName = prompt("Enter your last name:");
    var email = prompt("Enter your email:");
    var phone = prompt("Enter your phone number:");

    newContact = {};
    newContact.first = firstName;
    newContact.last = lastName;
    newContact.email = email;
    newContact.phone = phone;

    var duplicate = false;

    for(var i = 0; i < contacts.length; i++)
    {
        if(contacts[i].first === firstName && contacts[i].last === lastName)
        {
            alert("This name has already been used!");
            duplicate = true;
        }
    }

    if(!duplicate) {
        contacts.push(newContact);
        updateContacts();
    }
}
