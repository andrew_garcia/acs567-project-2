/*
 *The createNav function is used to create the navigation html at the top of the page.
 */
function createNav(){

	var alphabet = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

	for(var i = 0; i < alphabet.length; i++)
	{
		document.write("<a href=\"#" + alphabet[i] + "\">" + alphabet[i] + "</a>&nbsp&nbsp");
	}
}